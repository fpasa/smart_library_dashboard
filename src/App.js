import React, { Component } from "react"
import "./App.css"
import io from "socket.io-client"
import Door from "./components/Door"
import ShelfContainer from "./components/ShelfContainer"
import Shelf from "./components/Shelf"
import Book from "./components/Book"
import BorrowedBooks from "./components/BorrowedBooks"
import axios from "axios"
import _ from "lodash"
import MaterialIcon, {colorPallet} from 'material-icons-react';
  <MaterialIcon icon="book" />

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      doorStatus: "open",
      borrowedBooks: [
        /*{
          book: { title: "asdasd", rfid: 1, author: "topo gigio" },
          user: { name: "fra", telegram_id: 1 }
        },
        {
          book: { title: "asdas123123d", rfid: 2, author: "topo grigio" },
          user: { name: "gio", telegram_id: 2 }
      }*/
      ],
      pendingBadBooks: [],
      shelfedBooks: []
    }
  }

  componentDidMount() {
    const socket = io("http://127.0.0.1:5000")
    socket.on("connect", () => console.log("connected"))

    socket.on("book-shelfed", data => {
      if (undefined !== _.find(this.state.shelfedBooks, b => b.rfid == data.rfid)) {
          console.warn('nothing done');
          return;
      }

      var badData = _.find(this.state.pendingBadBooks, b => b.book.rfid == data.rdif);
      if (badData !== undefined) {
          data.bad = true;
          data.badCorrection = badData.homeShelves[0].position;
      } else {
          data.bad = false;
          data.badCorrection = '';
      }

      this.setState({
        shelfedBooks: [...this.state.shelfedBooks, data]
      })
      this.setState({ text: this.state.text + data })
      console.log("Book shelfed", data)
    })

    socket.on("book-unshelfed", book => {
      this.setState({
        shelfedBooks: _.filter(this.state.shelfedBooks, b => {
          return b.rfid != book.rfid
        })
      })

      console.log("Book unshelfed", book)
    })

    socket.on("book-borrowed", book => {
      if (undefined !== _.find(this.state.borrowedBooks, d => d.book.rfid == book.book.rfid)) {
          console.warn('nothing done');
          return;
      }

      this.setState({
        borrowedBooks: [...this.state.borrowedBooks, book]
      })
      console.log("Book borrowed", book)
    })

    socket.on("door-unlock", () => {
      console.log("unlocking door")
      this.setState({
        doorStatus: "open"
      })
    })

    socket.on("door-lock", () => {
      console.log("locking door")
      this.setState({ doorStatus: "close" })
    })

    socket.on("book-returned", book => {
      this.setState({
        borrowedBooks: _.filter(this.state.borrowedBooks, b => {
          return b.rfid != book.rfid
        })
      })

      console.log("Book returned", book)
    })

    socket.on("book-bad-shelfed", data => {
        var badPositionBookIdx = _.findIndex(this.state.shelfedBooks, b => {
            return data.book.rfid == b.rfid;
        });

        if (badPositionBookIdx >= 0) {
            let books = this.state.shelfedBooks.slice();
            books[badPositionBookIdx].bad = true;
            books[badPositionBookIdx].badCorrection = data.homeShelves[0].position;

            this.setState({
                shelfedBooks: books
            })
        } else {
            this.setState({
                pendingBadBooks: [...this.state.pendingBadBooks, data]
            })
        }

      console.log("Book bad position", data)
    })
  }

  render() {
    const groupedByShelfBooks = _.groupBy(this.state.shelfedBooks, "shelf")
    return (
      <div className="App">
        <nav
          className="App-navbar"
          role="navigation"
          aria-label="main navigation"
        >
          <h1 className="App-heading">SHELFY DASHBOARD</h1>
        </nav>
        <h2 className="Top-title">
            Shelves
        </h2>
        <div className="App-container">
          <main className="App-main">
            <Shelf className="column is-three-fifth">
              <ShelfContainer className="ShelfContainer" id="1">
                {groupedByShelfBooks[3]
                  ? groupedByShelfBooks[3].map((book, id) => {
                      return (
                        <Book
                          nfcId={book.nfcid}
                          key={id}
                          author={book.author}
                          title={book.title}
                          status={book.availability}
                          bad={book.bad}
                          correction={book.badCorrection}
                        />
                      )
                    })
                  : null}
              </ShelfContainer>
              <ShelfContainer className="ShelfContainer" id="2">
                {groupedByShelfBooks[2]
                  ? groupedByShelfBooks[2].map((book, id) => {
                      return (
                        <Book
                          nfcId={book.nfcid}
                          key={id}
                          author={book.author}
                          title={book.title}
                          status={book.availability}
                          bad={book.bad}
                          correction={book.badCorrection}
                        />
                      )
                    })
                  : null}
              </ShelfContainer>
              <ShelfContainer className="ShelfContainer" id="3">
                {groupedByShelfBooks[1]
                  ? groupedByShelfBooks[1].map((book, id) => {
                      return (
                        <Book
                          nfcId={book.nfcid}
                          key={id}
                          author={book.author}
                          title={book.title}
                          status={book.availability}
                          bad={book.bad}
                          correction={book.badCorrection}
                        />
                      )
                    })
                  : null}
              </ShelfContainer>
            </Shelf>

          </main>
          <aside className="App-aside">
            <Door doorStatus={this.state.doorStatus} />
          <h2 className="Borrow-title">Borrowed Books</h2>
            <BorrowedBooks
              className="App-books"
              books={this.state.borrowedBooks}
            />
          </aside>
        </div>
      </div>
    )
  }
}

export default App
