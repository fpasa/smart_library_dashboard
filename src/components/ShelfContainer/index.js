import React, { Component } from "react"
import "./style.css"
class ShelfContainer extends Component {
  render() {
    return <div className="ShelfContainer">{this.props.children}</div>
  }
}

export default ShelfContainer
