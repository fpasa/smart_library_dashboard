import React, { Component } from "react"
import "./style.css"
import bookUrl from "../../img/book.svg"
import _ from "lodash"
import MaterialIcon, {colorPallet} from 'material-icons-react';

class Book extends Component {
  constructor(props) {
    super(props)
    this.randomColors = [
      "#ce93d8",
      "#9fa8da",
      "#90caf9",
      "#81d4fa",
      "#80cbc4",
      "#e6ee9c",
      "#fff59d",
      "#ffe082",
      "#ffcc80",
      "#ffab91"
    ]
  }
  render() {
    let cssClasses = "Book"
    if (this.props.status !== "borrowed") {
            cssClasses += " Book-borrowed"
    }

    if (this.props.bad) {
            cssClasses += " Book-bad"
    }

    console.log("nfc id", this.props.nfcId)
    if (this.props.placeholder) {
      return (
        <div className={cssClasses}>
          <img
            src={bookUrl}
            style={{ backgroundColor: this.props.bad ? 'red' : this.randomColors[this.props.nfcId] }}
          />
          <div className="Book-title">
              <b />
              <span />
          </div>
          <div className="Book-author">
            <b />
            <span />
          </div>
          <div className="Book-msg">This book should be in the {this.props.correction} shelf</div>
        </div>
      )
    }
    return this.props.status !== "borrowed" ? (
      <div className={cssClasses}>
        <MaterialIcon
          icon="book"
          style={{ color: this.props.bad ? 'red' : this.randomColors[this.props.nfcId], }}
        />
        <div className="Book-title">
            <b>Title:</b>
            <span>{this.props.title}</span>
        </div>
        <div className="Book-author">
          <b>Author:</b>
          <span>{this.props.author}</span>
        </div>
        <div className="Book-msg">This book should be in the {this.props.correction} shelf</div>
      </div>
    ) : (
      <div className={cssClasses}>
      <MaterialIcon
          icon="book"
          style={{ color: this.randomColors[this.props.nfcId] }}
        />
        <div className="Book-title">
            <b>Title:</b>
            <span>{this.props.title}</span>
        </div>
        <div className="Book-author">
          <b>Author:</b>
          <span>{this.props.author}</span>
        </div>
        <div className="Book-msg">This book should be in the {this.props.correction} shelf</div>
        <div className="Book-borrowed-title">Borrowed</div>
      </div>
    )
  }
}

export default Book
