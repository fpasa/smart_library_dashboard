import "./style.css"
import React, { Component } from "react"
class Door extends Component {
  render() {
    const status = this.props.doorStatus;
    const classes = 'Door Door-' + status;

    return (
      <div className={classes}>
        <header className="Door-header">
          <h1 className="Door-title">The door is {status}</h1>
        </header>
        <main />
      </div>
    )
  }
}

export default Door
