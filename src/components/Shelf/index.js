import "./style.css"
import React, { Component } from "react"
class Shelf extends Component {
  render() {
    return <div className="Shelf">{this.props.children}</div>
  }
}

export default Shelf
