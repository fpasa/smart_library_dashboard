import React, { Component } from "react"
import "./style.css"
import _ from "lodash"
import MaterialIcon, {colorPallet} from 'material-icons-react';

class BorrowedBooks extends Component {
  render() {
    const groups = _.groupBy(this.props.books, d => {
      return d.user.telegram_id
    })

    let content = []
    _.forEach(groups, (group, key) => {
      var userContent = group.map(book => {
        return (
          <div className="BorrowedBook" key={book.book.rfid}>
            <MaterialIcon icon="book" />
            <h4>{book.book.title}</h4>
            <span>{book.book.author}</span>
          </div>
        )
      })

      content.push(
        <div className="User" key={key}>
            <header>
                <MaterialIcon className="iconLeft" icon="face" />
                <h1>{group[0].user.name}</h1>
            </header>
          <div>{userContent}</div>
        </div>
      )
    })

    if (!Object.keys(content).length) {
      content = null
    }

    return <div className="Borrow-container">{content}</div>
  }
}

export default BorrowedBooks
